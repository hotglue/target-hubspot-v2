"""HubspotV2 target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_hubspot_v2.sinks import HubspotV2Sink


class TargetHubspotV2(Target):
    """Sample target for HubspotV2."""

    name = "target-hubspot-v2"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("redirect_uri", th.StringType, required=True),
    ).to_dict()
    default_sink_class = HubspotV2Sink


if __name__ == "__main__":
    TargetHubspotV2.cli()
